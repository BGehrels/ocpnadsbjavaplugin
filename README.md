# ADS-B JavaScript Plugin for OpenCPN

The goal is to have a JavaScriptPlugin that can display intresting aircraft as routes on the chart.
This Repo holds the actual JavaScriptPlugin plus the files that are created by readsb. Without readsb there will be no tracks visible in OpenCPN but
they will be visible inside the JavaScriptPlugin.

## Requirements

1. OpenCPN
2. readsb

## Set up

1. start readsb in Terminal: `readsb --write-json="Documents/JavaScriptPlugin" --device-type rtlsdr`
2. Make sure the current version of the "javascript" Plugin is installed in OpenCPN
    1. Click Options (flywheel icon) -> Plugins
    2. Click "Update Plugin Catalog: master"
    3. Find the "javascript" Plugin in the list and click it.
    4. Click on install
3. Configure the openCPN JavaScript Plugin.
    1. Find the javascript plugin in the list again
    2. Make sure "Enabled" is checked
    3. Click on "Preferences"
    4. Switch to the "Directory" tab
    5. The directory is the same as "Documents/JavaScriptPlugin" (or what ever you have chosen for readsb before)
        - config.json also need to be in that folder
    6. Close the Dialog window and hit OK in the main settings dialog.
4. Load and tun our ADSB Tracker Plugin:
    1. click the "{JS}"-button in the tools panel
    2. click Load
    3. select the ADSBTrackerDialogeV1_5.js file
    4. Tick AutoRun if you wish to have the script run automatically on start up.
    5. Click Run
    6. Click the "{JS}"-button in the tools panel again to close the settings screen.

## General Function

The script will read every second the aircraft.json file created by readsb. It then tracks if an aircraft flies low or in a circle based on the
information provided by ADS-B.

If an aircraft is flying low an alarm sounds and a dialogue pops up. In case of a false alarm the flight can be set to be ignored for some time.
However if it only starts flying a cricle or comes closer after it was set to be ignored it will sound an alarm and popup a message again.

If the flight is not ticked, it will be imported as a route in openCPN. It can then be viewed and modified.

The dialogue will pop up again after a few seconds, this can be used to update the route of an aircraft. The dialogue can be pushed aside to be
working on the chart. However a new alarm is triggered the diaolgue will reopen.

Old routes, that where displayed on the map will be automatically imported and, if they are too old, removed.

If you are not sure if the programm is still running, click on the "JS" button in the OpenCPN menu. If there is a Stop button, the programm is still
running. Also check the terminal for readsb if it still running.

## Coniguration

the configuration can be made through config.json which looks like so:

```
{
   "deleteAfterSeconds":3600,	-oldest time for waypints of flight path to exist in seconds
   "altitudeThreshold":37000,	-all flights above this threshold are considered useless and wont be imported in any way
   "dialogueRefreshRate":60,	-refresh rate in seconds after which a dialogue pop ups again to update the route
   "waypointDistance":2,		-distance in seconds between waypoints. Too many waypoints clutter and slow down openCPN, to little make it impossible to recognize circles.
   "altitudeAlarmThreshold":37000,	-all flights below this threshold will sound an alarm
   "circleAlarmThreshold":200,	-threshold of after how many degrees turning the flight path is considered a circle (do not make 360, they also fly a figure of 8)
   "distanceAlarmThreshold":20,	-threshold of alarm if an airplane is really close
   "ignoredFlightNumberPrefixes": ["KLM", "LWA"] -ignores all flights that have a flight number starting with one if the given prefixes. Can be used to ignore all planes of one airline. 
}
```

A second configuration file is the knownAssetsList.csv in the same directory. If there are some aircrafts, that you are particularily interested in,
you can use that file to add a proper Callsign or name and some remarks for each ADSB hex code of interest.

## Development & Contributions

The JS Engine used by the OpenCPN JS plugin only supports a quite ancient variant of ECMAscript, according to
the [User Guide](https://github.com/antipole2/JavaScript_pi/blob/master/documentation/JavaScript_plugin_user_guide.pdf):

> The engine fully supports ECMAScript
> E5.1 with partial support for E6 and E7.
> Note that the embedded engine does not support:
> • for-of loops
> • classes
> • arrow functions

The module loading seems to also be very basic, we have seen issues with transitive requires, for example. Please make sure that all production code
is compatible with that.

The tests will be executed by a modern Node.js environment, so they can use modern js features.

If you have Node.js installed, tests can be run by calling

    npm i
    npm run test

There is no build / compile step for this project, the code written will be run as is.