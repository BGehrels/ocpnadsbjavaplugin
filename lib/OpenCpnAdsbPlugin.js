function OpenCpnAdsbPlugin(config, knownAssets, aircraftShouldBeIgnored, Route, Waypoint, OpenCpnApi, aircraftSource) {
  var flights = [];
  var alarmFlightIdx = []
  var ignoreFlightNames = []
  var textInfos = []
  var makeAlarm = false
  var dialogueOn = false
  var refreshDialogueTime = -1

  this.loopFlights = function () {
    loadFlights();
    alarm();
    if (refreshDialogueTime > -1 && new Date / 1000 > refreshDialogueTime && !dialogueOn) {
      popDialogue()
    }
  }

  var alarm = function () {
    for (var f = 0; f < flights.length; f++) {
      var flight = flights[f]
      flight.checkIgnore()
      if (flight.ignore) {
        continue
      }
      flight.setAlarm()
    }
    if (makeAlarm) {
      OpenCpnApi.soundAlarm()
      makeAlarm = false
      OpenCpnApi.onDialogue(false)
      dialogueOn = false
      popDialogue()
    }
  }

  var popDialogue = function () {
    alarmFlightIdx = []
    textInfos = []
    for (var f = 0; f < flights.length; f++) {
      var flight = flights[f]
      if (flight.checkAlarm()) {
        alarmFlightIdx.push(f)
        var text = flight.name + ": flying"
        if (flight.alarmLow) {
          text = text + " low (" + flight.altitude + "ft)"
        }
        if (flight.alarmClose) {
          text = text + " close by (" + flight.distance + "nm)"
        }
        if (flight.alarmCircle) {
          text = text + " in a circle!"
        }
        textInfos.push(text)
      }
    }

    if (textInfos.length > 0) {
      if (!dialogueOn) {
        dialogueOn = true;
        OpenCpnApi.onDialogue(
          handleDialogue,
          [
            { type: "caption", value: "ADS-B Alarm!" },
            {
              type: "text",
              value: "Select cases to be ignored.\nThe rest will be displayed as routes:"
            },
            { type: "tickList", value: textInfos },
            { type: "slider", range: [1, 240], value: 10, label: "ignore for" },
            { type: "text", value: "minuetes" },
            { type: "button", label: ["ignore selected", "ignore all"] }
          ]
        )
      }
    }
  }

  var handleDialogue = function (dialogue) {
    dialogueOn = false
    ignoreFlightNames = []
    if (dialogue[dialogue.length - 1].label === "ignore all") {
      for (var f = 0; f < textInfos.length; f++) {
        ignoreFlightNames.push(textInfos[f].slice(0, textInfos[f].indexOf(":")))
      }
    } else {
      for (f = 0; f < dialogue[2].value.length; f++) {
        ignoreFlightNames.push(dialogue[2].value[f].slice(0, dialogue[2].value[f].indexOf(":")))
      }
    }
    if (ignoreFlightNames.length > 0) {
      ignoreFlights(ignoreFlightNames, true, dialogue[3].value)
    }
    for (var a = 0; a < alarmFlightIdx.length; a++) {
      var flight = flights[alarmFlightIdx[a]]
      if (!flight.ignore) {
        flight.plotRoute()
      }
    }
    OpenCpnApi.refreshCanvas()
    refreshDialogueTime = new Date / 1000 + config.dialogueRefreshRate
  }

  var ignoreFlights = function (names, val, min) {
    var idx = []
    for (var a = 0; a < alarmFlightIdx.length; a++) {
      var flight = flights[alarmFlightIdx[a]]
      for (var n = 0; n < names.length; n++) {
        if (names[n].indexOf(flight.name) > -1) {
          flight.setIgnore(val)
          flight.deleteRoute()
          flight.ignoreUntil = flight.now + (min * 60)
          idx.push(a)
        }
      }
    }
    for (var i = 0; i < idx.length; i++) {
      alarmFlightIdx.slice(idx[i], 1)
    }
  }

  var init = function () {
    var guids = OpenCpnApi.getRouteGUIDs()
    for (var r = 0; r < guids.length; r++) {
      var route = OpenCpnApi.getRoute(guids[r])
      if (route.name.indexOf("Flight") > -1) {
        var flightName = route.name.slice(route.name.indexOf(": ") + 2, route.name.length);
        var aircraft = {
          'flight': flightName,
          'alt_baro': "",
          'true_heading': [],
          'lat': 0
        };
        var flight = new FlightInfo(aircraft, undefined, undefined, this, OpenCpnApi, Route, Waypoint)
        flight.addRoute(route)
        if (flight.checkRoute()) {
          var description = route.waypoints[route.waypoints.length - 1].description
          var a = description.indexOf("Altitude: ") + 10
          var b = description.indexOf("ft")
          flight.altitude = description.slice(a, b);
          flight.f = r
          var fix = OpenCpnApi.getNavigation()
          var vec = OpenCpnApi.getVectorPP(fix.position, route.waypoints[route.waypoints.length - 1].position)
          flight.distance = vec.distance
          flight.accDiffHeading = calcCircle(route.waypoints)
          flights.push(flight)
        }
      }
    }
  }

  var loadFlights = function () {
    var aircraftsObj = aircraftSource.refetch();
    var now = aircraftsObj.now
    for (var a = 0; a < aircraftsObj.aircraft.length; a++) {
      var aircraft = aircraftsObj.aircraft[a];
      if (!validAircraft(aircraft) || aircraftShouldBeIgnored(aircraft, config)) {
        continue;
      }
      var existingFlight = null;
      for (var f = 0; f < flights.length; f++) {
        if (aircraft.flight.indexOf(flights[f].name) > -1) {
          existingFlight = flights[f];
          break
        }
      }

      if (existingFlight === null) {
        var flight = new FlightInfo(aircraft, knownAssets.findByHexId(aircraft.hex), now, this, OpenCpnApi, Route, Waypoint);
        flight.update(aircraft, now);
        flight.f = flights.length;
        flights.push(flight);
      } else {
        existingFlight.update(aircraft, now)
      }
    }
  }

  function FlightInfo(aircraft, knownAssetsListEntry, _now, Plugin, OpenCpnApi, Route, Waypoint) {
    this.f = -1
    this.now = _now
    this.ignoreUntil = -1
    this.name = aircraft.flight
    this.heading = -1
    this.altitude = -40000
    this.route = new Route()
    this.route.name = "Flight: " + aircraft.flight
    this.accDiffHeading = 0
    this.distance = -1
    this.alarmCircle = false
    this.alarmLow = false
    this.alarmClose = false
    this.ignore = false

    this.update = function (aircraft, _now) {
      this.now = _now;
      var trueNow = new Date() / 1000
      if ((trueNow - (this.now - aircraft.seen)) > config.waypointDistance) {
        if (typeof aircraft.alt_baro != "undefined") {
          this.altitude = aircraft.alt_baro
        }

        if (this.route.waypoints.length > 0) {
          this.route.waypoints[this.route.waypoints.length - 1].useMinScale = true
          this.route.waypoints[this.route.waypoints.length - 1].minScale = 800
        }

        var waypoint = new Waypoint(aircraft.lat, aircraft.lon)
        waypoint.iconName = "Airplane"
        waypoint.markName = (knownAssetsListEntry || {}).opsCallsign || aircraft.flight
        waypoint.isNameVisible = true
        waypoint.creationDateTime = this.now - aircraft.seen
        waypoint.description = "Created by ADS-B" + "\nAltitude: " + aircraft.alt_baro + "ft\nHeading: " + aircraft.true_heading + "\nOperator and Comments: " + (knownAssetsListEntry || {}).operatorAndComments
        var fix = OpenCpnApi.getNavigation()
        var vec = OpenCpnApi.getVectorPP(fix.position, waypoint.position)
        this.distance = vec.distance

        var preHeading = this.heading
        if (aircraft.true_heading <= 360 && aircraft.true_heading > 0) {
          this.heading = aircraft.true_heading;
        } else if (this.route.waypoints.length > 0) {
          vec = OpenCpnApi.getVectorPP(this.route.waypoints[this.route.waypoints.length - 1].position, waypoint.position)
          if (vec.distance > 0) {
            this.heading = vec.bearing
          }
        }
        if (preHeading > -1) {
          var diff = this.heading - preHeading
          if (diff > 180) {
            diff = diff - 360
          } else if (diff < -180) {
            diff = 360 + diff
          }
          this.accDiffHeading = this.accDiffHeading + diff
        }
        this.route.waypoints.push(waypoint)
      }
      this.checkRoute()
    }

    this.setAlarm = function () {
      if (this.route.waypoints.length > 2) {
        if ((this.accDiffHeading < -config.circleAlarmThreshold) || this.accDiffHeading > config.circleAlarmThreshold) {
          if (!this.alarmCircle) {
            makeAlarm = true
          }
          this.alarmCircle = true
        }
        if (this.altitude > -40000 && this.altitude < config.altitudeAlarmThreshold) {
          if (!this.alarmLow) {
            makeAlarm = true
          }
          this.alarmLow = true
        } else {
          this.alarmLow = false
        }
        if (this.distance > -1 && this.distance < config.distanceAlarmThreshold) {
          if (!this.alarmClose) {
            makeAlarm = true
          }
          this.alarmClose = true
        } else {
          this.alarmClose = false
        }
      } else {
        this.alarmCircle = false;
        this.alarmLow = false;
        this.alarmClose = false;
        makeAlarm = false;
      }
    }

    this.checkAlarm = function () {
      if (this.route.waypoints.length > 2) {
        return this.alarmLow || this.alarmCircle || this.alarmClose;
      } else {
        return false
      }
    }

    this.setIgnore = function (val) {
      this.ignore = val
    }

    this.checkIgnore = function () {
      if (!this.ignore) {
        return
      }
      if (this.now > this.ignoreUntil) {
        this.ignore = false
      }
    }

    this.addRoute = function (_route) {
      this.route = _route
    }

    this.deleteRoute = function () {
      if (this.route.GUID.length !== 0 || this.validGUID()) {
        OpenCpnApi.deleteRoute(this.route.GUID)
        this.route.GUID = ""
      }
    }

    this.plotRoute = function () {
      if (this.route.GUID.length === 0 || !this.validGUID()) {
        if (this.route.waypoints.length > 2) {
          this.route.GUID = OpenCpnApi.addRoute(this.route)
        }
      } else {
        if (this.route.waypoints.length < 2) {
          OpenCpnApi.deleteRoute(this.route.GUID)
          this.route.GUID = ""
          flights.splice(this.f, 1)
        } else {
          OpenCpnApi.updateRoute(this.route)
        }
      }
    }

    this.checkRoute = function () {
      var now = new Date() / 1000
      if (now - this.now > config.deleteAfterSeconds) {
        this.deleteFlight()
        return false;
      }
      if (this.route.waypoints.length > 0 && now - this.route.waypoints[0].creationDateTime > config.deleteAfterSeconds) {
        var w;
        for (w = 0; w < this.route.waypoints.length; w++) {
          var waypoint = this.route.waypoints[w]
          if (now < waypoint.creationDateTime + config.deleteAfterSeconds) {
            break
          }
        }
        this.route.waypoints.splice(0, w)
        if (this.route.waypoints.length === 0) {
          this.deleteFlight()
          return false;
        }
      }

      if (this.altitude > config.altitudeThreshold &&
        !this.alarmLow &&
        !this.alarmCircle &&
        !this.alarmClose) {
        this.deleteFlight()
        return false;
      }
      return true;
    }

    this.deleteFlight = function () {
      if (this.route.GUID.length > 0) {
        if (this.validGUID()) {
          OpenCpnApi.deleteRoute(this.route.GUID)
          this.route.GUID = ""
          OpenCpnApi.refreshCanvas()
          if (this.f > -1) {
            flights.splice(this.f, 1)
            Plugin.correctFlightIdx()
          }
        } else {
          if (this.f > -1) {
            flights.splice(this.f, 1)
            Plugin.correctFlightIdx()
          }
        }
      } else {
        if (this.f > -1) {
          flights.splice(this.f, 1)
          Plugin.correctFlightIdx()
        }
      }
    }

    this.validGUID = function () {
      var guids = OpenCpnApi.getRouteGUIDs()
      var guidIsValid = false
      for (var g = 0; g < guids.length; g++) {
        if (this.route.GUID === guids[g]) {
          guidIsValid = true
          break
        }
      }
      return guidIsValid
    }
  }

  var calcCircle = function (waypoints) {
    var accDiffHeading = 0
    if (waypoints.length > 2) {
      var vec1 = OpenCpnApi.getVectorPP(waypoints[0].position, waypoints[1].position)
      for (var w = 2; w < waypoints.length; w++) {
        var vec2 = OpenCpnApi.getVectorPP(waypoints[w - 1].position, waypoints[w].position)
        if (vec1.distance > 0 && vec2.distance > 0) {
          var diff = calcDiff(vec1.bearing, vec2.bearing)
          accDiffHeading = accDiffHeading + diff
          vec1 = vec2
        }
      }
    }
    return accDiffHeading
  }

  var calcDiff = function (pre, next) {
    var diff = next - pre
    if (diff > 180) {
      diff = diff - 360
    } else if (diff < -180) {
      diff = 360 + diff
    }
    return diff
  }

  var validAircraft = function (aircraft) {
    var valid = true;
    if (aircraft.lat === "-2147483648&#xb0 -214748.-3648' N") {
      valid = false
    } else if (typeof aircraft.lat == "undefined") {
      valid = false
    } else if (typeof aircraft.flight == "undefined") {
      valid = false
    } else if (aircraft.flight === "") {
      valid = false
    }
    return valid
  }

  this.correctFlightIdx = function () {
    for (var f = 0; f < flights.length; f++) {
      flights[f].f = f
    }
  }


  OpenCpnApi.consoleHide();
  init();
}

module.exports = OpenCpnAdsbPlugin;