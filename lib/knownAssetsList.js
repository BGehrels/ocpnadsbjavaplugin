function KnownAssetsList(rawCsvString) {
  const parsedAssets = {};
  rawCsvString
    .split(/\r?\n/)
    .map(function (line) { return line.trim(); })
    .filter(function (line) { return line !== ''; })
    .map(function (line) { return line.split(/\s*;\s*/); })
    .filter(function (lineArray, lineIdx) { return !(lineIdx === 0 && lineArray[0] === 'Hex'); })
    .forEach(function (lineArray) { parsedAssets[lineArray[0].toLowerCase()] = { hex: lineArray[0], opsCallsign: lineArray[1], operatorAndComments: lineArray[2] }; });


  this.findByHexId = function (hexId) {
    return parsedAssets[hexId.toLowerCase()] || undefined;
  }
}

module.exports = KnownAssetsList;