Route = require("Route");
Waypoint = require("Waypoint");
aircraftShouldBeIgnored = require("lib/aircraftShouldBeIgnored.js");
KnownAssetsList = require('lib/knownAssetsList.js');
OpenCpnAdsbPlugin = require('lib/OpenCpnAdsbPlugin.js');

var config = JSON.parse(readTextFile("config.json"));
var knownAssets = new KnownAssetsList(readTextFile("knownAssetsList.csv"));

var OpenCPNApi = {
  onSeconds: onSeconds,
  soundAlarm: OCPNsoundAlarm,
  onDialogue: onDialogue,
  refreshCanvas: OCPNrefreshCanvas,
  getRouteGUIDs: OCPNgetRouteGUIDs,
  getRoute: OCPNgetRoute,
  getNavigation: OCPNgetNavigation,
  getVectorPP: OCPNgetVectorPP,
  addRoute: OCPNaddRoute,
  deleteRoute: OCPNdeleteRoute,
  updateRoute: OCPNupdateRoute,
  consoleHide: consoleHide
}

var AircraftSource = {
  refetch: function() {
    return JSON.parse(readTextFile(config.aircraftFile))
  }
}

// The JS Interpreter used by OpenCPNs JavaScript Plugin only has a very, very basic support for require and modules.
// Transitive requires seem not to be supported. so we have to import everything on the upper level and then pass it as Parameters
var openCpnPlugin = new OpenCpnAdsbPlugin(config, knownAssets, aircraftShouldBeIgnored, Route, Waypoint, OpenCPNApi, AircraftSource);


function loop() {
  openCpnPlugin.loopFlights();
  onSeconds(loop, 1);
}

loop();
